<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Short;

use App\Domain\Short\Short;
use App\Domain\Short\ShortExistsException;
use App\Domain\Short\ShortNotFoundException;
use App\Domain\Short\ShortRepository;

class LocalFileRepository implements ShortRepository
{
    /**
     * @var Short[]
     */
    private array $shorts;

    /**
     * @param Short[]|null $users
     */
    public function __construct(array $shorts = null)
    {
        if ( !file_exists('../data/shorts.json') ) {
            $this->shorts = [];
            file_put_contents('../data/shorts.json', json_encode($this->shorts));
        }
        else {
            $raw = json_decode(file_get_contents('../data/shorts.json'), true);
            $this->shorts = [];
            foreach ( $raw as $r ) {
                $this->shorts[$r['id']] = new Short($r['id'], $r['uid'], $r['target'], $r['created'], $r['requests']);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        return array_values($this->shorts);
    }

    public function findById(string $id): Short
    {
        if ( !array_key_exists($id, $this->shorts) ) {
            throw new ShortNotFoundException();
        }

        return $this->shorts[$id];
    }

    public function findByUid(string $uid, bool $silent = false): Short|bool
    {
        foreach ( $this->shorts as $short) {
            if ( $short->getUid() == $uid ) {
                return $short;
            }
        }
        if ( $silent ) {
            return false;
        }
        throw new ShortNotFoundException();
    }

    public function findByTarget(string $target, bool $silent = false): Short|bool
    {
        foreach ( $this->shorts as $short) {
            if ( $short->getTarget() == $target ) {
                return $short;
            }
        }
        if ( $silent ) {
            return false;
        }
        throw new ShortNotFoundException();
    }

    public function create(string $target): Short
    {
        foreach ( $this->shorts as $short ) {
            if ( $short->getTarget() === $target ) {
                throw new ShortExistsException();
            }
        }

        $id = intval(array_key_last($this->shorts)) + 1;
        $uid = uniqid();

        $short = new Short($id, $uid, $target, time(), []);

        $this->shorts[$id] = $short->jsonSerialize();
        $this->persist();

        return $short;
    }

    public function requested(Short $short)
    {
        foreach ( $this->shorts as $key => $s ) {
            if ( $s->getId() == $short->getId() ) {
                $this->shorts[$key]->addRequest();
            }
        }
        $this->persist();
    }

    private function persist() {
        file_put_contents('../data/shorts.json', json_encode($this->shorts));
    }
}
