<?php

declare(strict_types=1);

namespace App\Application\Actions\Short;

use Psr\Http\Message\ResponseInterface as Response;

class GetRequestsAction extends ShortAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $id = $this->resolveArg('id');
        $short = $this->shortRepository->findById($id);

        //$this->logger->info("User of id `${userId}` was viewed.");

        $resData = [
            'total' => count($short->getRequests()),
            'requests' => $short->getRequests()
        ];
        return $this->respondWithData($resData);
    }
}
