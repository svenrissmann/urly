<?php

declare(strict_types=1);

namespace App\Application\Actions\Short;

use Psr\Http\Message\ResponseInterface as Response;

class DetailAction extends ShortAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        //$uid = $this->resolveArg('uid');
        //$short = $this->shortRepository->findByUid($uid);

        $target = $this->request->getQueryParams()['target'];
        $short = $this->shortRepository->findByTarget($target);

        //$this->logger->info("User of id `${userId}` was viewed.");

        return $this->respondWithData($short);
    }
}
