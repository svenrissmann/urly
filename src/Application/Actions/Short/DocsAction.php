<?php

declare(strict_types=1);

namespace App\Application\Actions\Short;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\Twig;

class DocsAction extends ShortAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $view = Twig::fromRequest($this->request);
        $jsonFile = __DIR__ . '/../../../../resources/docs/swagger.json';
        return $view->render($this->response, 'swagger.html', [
            'spec' => file_get_contents(($jsonFile))
        ]);
    }
}
