<?php

declare(strict_types=1);

namespace App\Application\Actions\Short;

use App\Application\Actions\Action;
use App\Domain\Short\ShortRepository;
use Psr\Log\LoggerInterface;

abstract class ShortAction extends Action
{
    protected ShortRepository $shortRepository;

    public function __construct(LoggerInterface $logger, ShortRepository $shortRepository)
    {
        parent::__construct($logger);
        $this->shortRepository = $shortRepository;
    }
}
