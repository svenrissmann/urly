<?php

declare(strict_types=1);

namespace App\Application\Actions\Short;

use App\Application\Actions\ActionError;
use Psr\Http\Message\ResponseInterface as Response;

class CreateAction extends ShortAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $target = $this->getFormData()['target'];
        if ( !filter_var($target, FILTER_VALIDATE_URL) ) {
            $error = new ActionError('VALIDATION_ERROR', 'The given target is not a valid URL.');
            return $this->respondWithData($error, 400);
        }

        if ( $short = $this->shortRepository->findByTarget($target, true) ) {
            $resData = $short->jsonSerialize();
            $resData['existing'] = true;
            return $this->respondWithData($resData);
        }

        $short = $this->shortRepository->create($target);

        return $this->respondWithData($short->jsonSerialize());
    }
}
