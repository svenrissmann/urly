<?php

declare(strict_types=1);

namespace App\Application\Actions\Short;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\Twig;

class RequestAction extends ShortAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $uid = ltrim($this->request->getRequestTarget(), '/');

        $short = $this->shortRepository->findByUid($uid, true);

        if ( $short ) {
            $this->shortRepository->requested($short);
            return $this->response->withHeader('Location', $short->getTarget());
        }
        else {
            $view = Twig::fromRequest($this->request);
            return $view->render($this->response, '404.html', [
                'uid' => $uid
            ]);
        }
    }
}
