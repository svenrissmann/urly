<?php

declare(strict_types=1);

namespace App\Domain\Short;

use App\Domain\DomainException\DomainRecordNotFoundException;

class ShortExistsException extends DomainRecordNotFoundException
{
    public $message = 'A short with given target already exists.';
}
