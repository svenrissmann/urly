<?php

declare(strict_types=1);

namespace App\Domain\Short;

use App\Domain\DomainException\DomainRecordNotFoundException;

class ShortNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The short you requested does not exist.';
}
