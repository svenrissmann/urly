<?php

declare(strict_types=1);

namespace App\Domain\Short;

use JsonSerializable;

class Short implements JsonSerializable
{
    private ?int $id;

    private string $uid;

    private string $target;

    private int $created;

    private array $requests;

    public function __construct(?int $id, string $uid, string $target, int $created, array $requests)
    {
        $this->id = $id;
        $this->uid = $uid;
        $this->target = $target;
        $this->created = $created;
        $this->requests = $requests;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getTarget(): string
    {
        return $this->target;
    }

    public function getCreated(): string
    {
        return $this->target;
    }

    public function getRequests(): array
    {
        return $this->requests;
    }

    public function addRequest()
    {
        $this->requests[] = [
            'time' => time()
        ];
    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'uid' => $this->uid,
            'target' => $this->target,
            'created' => $this->created,
            'requests' => $this->requests,
        ];
    }
}
