<?php

declare(strict_types=1);

namespace App\Domain\Short;

interface ShortRepository
{
    /**
     * @return Short[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return Short
     * @throws UserNotFoundException
     */
    public function findById(string $id): Short;

    public function findByUid(string $uid, bool $silent): Short|bool;

    public function findByTarget(string $target, bool $silent): Short|bool;

    public function create(string $target): Short;

    public function requested(Short $short);
}
