<?php

declare(strict_types=1);

use App\Application\Actions\Short\CreateAction;
use App\Application\Actions\Short\DetailAction;
use App\Application\Actions\Short\ListAction;
use App\Application\Actions\Short\RequestAction;
use App\Application\Actions\Short\DocsAction;
use App\Application\Actions\Short\GetRequestsAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use User\ViewShortAction;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->group('/users', function (Group $group) {
        $group->get('', ListUsersAction::class);
        $group->get('/{id}', ViewShortAction::class);
    });

    $app->group('/api/v1/short', function (Group $group) {
        //$group->get('', ListAction::class);
        $group->get('', DetailAction::class);
        $group->post('', CreateAction::class);
        $group->get('/{id}/requests', GetRequestsAction::class);
    });

    $app->get('/api-docs/v1', DocsAction::class);

    $app->get('/{uid}', RequestAction::class);

};
