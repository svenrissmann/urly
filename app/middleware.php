<?php

declare(strict_types=1);

use App\Application\Middleware\SessionMiddleware;
use Slim\App;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

return function (App $app) {
    $app->add(SessionMiddleware::class);
    // Create Twig
    $twig = Twig::create('../resources/templates', ['cache' => false]);
    $app->add(TwigMiddleware::create($app, $twig));
};
